#include "../inc/controle.h"

float TE = 0.0, TI = 0.0, TR = 0.0;
int temp;

void printar_tela(float TE, float TI, float TR, double controle){
    printf("Para finalizar o controle, pressione ctrl + C\n");
    printf("TE:%.2f  TI:%.2f  TR:%.2f, Controle:%.2f\n", TE, TI, TR, controle);
}

int onoffpotenciometro(int f_uart0, float histerese){
    while (1){
        double controle;    
        getInformationBME280(&temp);
            TE = (float)temp/100;
        solicita_temperatura_interna(f_uart0, &TI);
        solicita_temperatura_potenciometro(f_uart0, &TR);
        print_temp(TE, TI, TR);
        if (TI > TR + histerese/2){
            controle = -100;
            envia_sinal_controle(f_uart0, controle);
            controle_resistencia_ventoinha(controle);
        } else if (TI < TR - histerese/2){
            controle = 100;
            envia_sinal_controle(f_uart0, controle);
            controle_resistencia_ventoinha(controle);
        } else {
            controle = 0;
            envia_sinal_controle(f_uart0, controle);
            controle_resistencia_ventoinha(controle);
        }

        printar_tela(TE, TI, TR, controle);
        adicionar_linha(TI, TE, TR, controle);
    }
}

int onoffmanual(int f_uart0, float TR, float histerese){
    while (1){
        double controle;    
        getInformationBME280(&temp);
            TE = (float)temp/100;
        solicita_temperatura_interna(f_uart0, &TI);
        print_temp(TE, TI, TR);
        if (TI > TR + histerese/2){
            controle = -100;
            envia_sinal_controle(f_uart0, controle);
            controle_resistencia_ventoinha(controle);
        } else if (TI < TR - histerese/2){
            controle = 100;
            envia_sinal_controle(f_uart0, controle);
            controle_resistencia_ventoinha(controle);
        } else {
            controle = 0;
            envia_sinal_controle(f_uart0, controle);
            controle_resistencia_ventoinha(controle);
        }

        printar_tela(TE, TI, TR, controle);
        adicionar_linha(TI, TE, TR, controle);
    }
}

void pidpotenciometro(int f_uart0, double Kp_, double Ki_, double Kd_){
    pid_configura_constantes(Kp_, Ki_, Kd_);
    while (1){
        getInformationBME280(&temp);
        TE = (float)temp/100;
        solicita_temperatura_interna(f_uart0, &TI);
        solicita_temperatura_potenciometro(f_uart0, &TR);
        print_temp(TE, TI, TR);        
        
        pid_atualiza_referencia(TR);
        double controle_pid = pid_controle(TI);
        envia_sinal_controle(f_uart0, controle_pid);
        controle_resistencia_ventoinha(controle_pid);        
        
        printar_tela(TE, TI, TR, controle_pid);
        adicionar_linha(TI, TE,  TR, controle_pid);
    }
}


void pidmanual(int f_uart0, float TR, double Kp_, double Ki_, double Kd_){
    pid_configura_constantes(Kp_, Ki_, Kd_);
    while (1){
        getInformationBME280(&temp);
        TE = (float)temp/100;
        solicita_temperatura_interna(f_uart0, &TI);
        print_temp(TE, TI, TR);        
        
        pid_atualiza_referencia(TR);
        double controle_pid = pid_controle(TI);
        envia_sinal_controle(f_uart0, controle_pid);
        controle_resistencia_ventoinha(controle_pid);        
        
        printar_tela(TE, TI, TR, controle_pid);
        adicionar_linha(TI, TE,  TR, controle_pid);
    }
}

int chavepotenciometro(int f_uart0, float histerese, double Kp_, double Ki_, double Kd_){
    int chave = -2;
    while (chave != 0 && chave != 1){
        chave = solicita_estado_chave(f_uart0);
        sleep(1);
    }
    
    if (chave == 0){
        printf("Controle ON/OFF\n");
        onoffpotenciometro(f_uart0, histerese);
    } else {
        printf("Controle PID\n");
        pidpotenciometro(f_uart0, Kp_, Ki_, Kd_);
    }
}

int chavemanual(int f_uart0, float TR, float histerese, double Kp_, double Ki_, double Kd_){
    int chave = -2;
    while (chave != 0 && chave != 1){
        chave = solicita_estado_chave(f_uart0);
        sleep(1);
    }
    
    if (chave == 0){
        printf("Controle ON/OFF\n");
        onoffmanual(f_uart0, TR, histerese);
    } else if (chave == 1){
        printf("Controle PID\n");
        pidmanual(f_uart0, TR, Kp_, Ki_, Kd_);
    }
}