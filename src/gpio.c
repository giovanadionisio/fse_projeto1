#include "../inc/gpio.h"

void controle_resistencia_ventoinha (int sinal_controle) {
    if (sinal_controle > 0) {
        desligar_ventoinha();
        ligar_resistencia((int) sinal_controle);
    }
    else {
        if (sinal_controle < -40) {
            ligar_ventoinha((int) (-1.0 * sinal_controle));
        }
        else {
            desligar_ventoinha();
        }

        desligar_resistencia();
    }
}

void ligar_resistencia(int potencia){
  pinMode(PIN_RESISTOR, OUTPUT);
  softPwmCreate(PIN_RESISTOR,  VALOR_INICIAL, RANGE);
  softPwmWrite(PIN_RESISTOR, potencia);
}

void ligar_ventoinha(int potencia){
  pinMode(PIN_VENTOINHA,OUTPUT);
  softPwmCreate(PIN_VENTOINHA, VALOR_INICIAL, RANGE);
  softPwmWrite(PIN_VENTOINHA, potencia);
}

void desligar_resistencia(){
  softPwmCreate(PIN_RESISTOR, VALOR_INICIAL, RANGE);
  pinMode(PIN_RESISTOR,OUTPUT);
  softPwmWrite(PIN_RESISTOR, VALOR_INICIAL);
}

void desligar_ventoinha(){
  pinMode(PIN_VENTOINHA,OUTPUT);
  softPwmCreate(PIN_VENTOINHA, VALOR_INICIAL, RANGE);
  softPwmWrite(PIN_VENTOINHA, VALOR_INICIAL);
}
