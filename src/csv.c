#include "../inc/csv.h"

void abrirCSV() {
    FILE* arq;

    arq = fopen("relatorio/relatorio.csv", "a+");

    fseek(arq, 0, SEEK_END);
    if(ftell(arq) - 1 <= 0){
        fprintf(arq, "timestamp, TE, TI, TR, controle\n");
    }  
    
    fclose(arq);
}

void adicionar_linha(float TI, float TE, float TR, double controle) {
    FILE* arq;
    time_t horario;
    struct tm *timestamp;
    char buffer[80];

    arq = fopen("relatorio/relatorio.csv", "a+");

    time(&horario);
    timestamp = localtime(&horario);

    strftime(buffer,80,"%F %X",timestamp);

    fprintf(arq, "%s, %.3f, %.3f, %.3f, %.3lf\n", buffer, TE, TI, TR, controle);

    fclose(arq);
}