#include "../inc/main.h"


void trata_sinal(int s){
    if(s == SIGINT){
        desligar_ventoinha();
        desligar_resistencia();
        close(f_uart0);
        exit(0);
    }
}
int main(){
    signal(SIGINT, trata_sinal);
    
    f_uart0 = iniciar_uart();
    if (f_uart0 == -1){
        printf("Falha ao acessar a UART!\n");
        return 0;
    }
    
    int i = bme280Init(1, 0x76);
    abrirCSV();
    wiringPiSetup();
    desligar_ventoinha();
    desligar_resistencia();

    printa_cabecalho();
    menu_referencia();
    
    return 0;
}