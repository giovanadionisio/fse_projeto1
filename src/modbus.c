#include "../inc/modbus.h"

int le_temperatura_interna(int f_uart0, float *TI){
    usleep(15000);
    int endereco, comando, subcomando;
    float temperatura_interna;
    short crc;

    int r = read(f_uart0, &endereco, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &comando, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &subcomando, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &temperatura_interna, 4);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &crc, 2);
    if (r == -1){
        return -2;
    }

    unsigned char aux[7] = {endereco, comando, subcomando};
    memcpy(&aux[3], &temperatura_interna, 4);

    short calcular_crc = calcula_CRC(aux, 7);
    
    if(calcular_crc != crc){
        return -3;
    } else {
        memcpy(TI, &temperatura_interna, 4);
        return 0;
    }
}

int solicita_temperatura_interna(int f_uart0, float *TI){
    unsigned char solicitar_temperatura_interna[7] = {0x01, 0x23, 0xC1, 7, 6, 5, 9};
    short crc = calcula_CRC(solicitar_temperatura_interna, 7);
    char solicita_crc[2];

    memcpy(&solicita_crc, &crc, 2);

    int w = write(f_uart0, &solicitar_temperatura_interna, sizeof(solicitar_temperatura_interna));
    if (w == -1){
        return -1;
    }
    w = write(f_uart0, &solicita_crc, 2);
    if(w == -1){
        return -1;
    } else {
        return le_temperatura_interna(f_uart0, TI);
    }
}

int le_temperatura_potenciometro(int f_uart0, float *TR){
    usleep(10000);

    int endereco, comando, subcomando;
    float temperatura_referencia;
    short crc;

    int r = read(f_uart0, &endereco, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &comando, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &subcomando, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &temperatura_referencia, 4);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &crc, 2);
    if (r == -1){
        return -2;
    }

    unsigned char aux[7] = {endereco, comando, subcomando};
    memcpy(&aux[3], &temperatura_referencia, 4);

    short calcular_crc = calcula_CRC(aux, 7);
    
    if(calcular_crc != crc){
        return -3;
    } else {
        memcpy(TR, &temperatura_referencia, 4);
        return 0;
    }
}

int solicita_temperatura_potenciometro(int f_uart0, float *TR){
    unsigned char solicitar_temperatura_potenciometro[7] = {0x01, 0x23, 0xC2, 7, 6, 5, 9};
    short crc = calcula_CRC(solicitar_temperatura_potenciometro, 7);
    char solicita_crc[2];

    memcpy(&solicita_crc, &crc, 2);

    int w = write(f_uart0, &solicitar_temperatura_potenciometro, sizeof(solicitar_temperatura_potenciometro));
    if (w == -1){
        return -1;
    }
    w = write(f_uart0, &solicita_crc, 2);

    if(w == -1){
        return -1;
    } else {
        return le_temperatura_potenciometro(f_uart0, TR);
    }
}

int le_estado_chave(int f_uart0){
    int endereco, comando, subcomando;
    int chave;
    short crc;

    int r = read(f_uart0, &endereco, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &comando, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &subcomando, 1);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &chave, 4);
    if (r == -1){
        return -2;
    }
    r = read(f_uart0, &crc, 2);
    if (r == -1){
        return -2;
    }

    char aux[7] = {endereco, comando, subcomando};
    memcpy(&aux[3], &chave, 4);

    short calcular_crc = calcula_CRC(aux, 7);

    if (calcular_crc == crc){
        return chave;
    } else {
        return -3;
    }
    
}

int solicita_estado_chave(int f_uart0){
    unsigned char solicitar_estado_chave[9] = {0x01, 0x23, 0xC3, 7, 6, 5, 9};
    short crc = calcula_CRC(solicitar_estado_chave, 7);

    memcpy(&solicitar_estado_chave[7], &crc, 2);

    int w = write(f_uart0, &solicitar_estado_chave, 9);
    if(w == -1){
        return -1;
    } else {
        return le_estado_chave(f_uart0);
    }    
}

int envia_sinal_controle(int f_uart0, int sinal){
    unsigned char envia[11] = {0x01, 0x23, 0xD1, 7, 6, 5, 9};
    memcpy(&envia[7], &sinal, 4);
    short crc = calcula_CRC(envia, 11);
    char solicita_crc[2];

    memcpy(&solicita_crc, &crc, 2);

    int w = write(f_uart0, &envia, sizeof(envia));
    if (w == -1){
        return -1;
    }
    w = write(f_uart0, &solicita_crc, 2);
    if (w == -1){
        return -1;
    }
}