#include "../inc/fuart.h"

int iniciar_uart(){
    int f_uart0 = open("/dev/serial0", O_RDWR | O_NDELAY | O_NOCTTY);
    if(f_uart0 == -1){
        return -1;
    }

    struct termios options;
    tcgetattr(f_uart0, &options); 
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR; 
    options.c_oflag = 0;
    options.c_lflag = 0;
    
    tcflush(f_uart0, TCIFLUSH);
    tcsetattr(f_uart0, TCSANOW, &options);

    return f_uart0;
}