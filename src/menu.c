#include "../inc/menu.h"

int f_uart0;
double Kp_, Ki_, Kd_;

void printa_cabecalho(){
    printf("============= PROJETO 01 =============\n");
    printf("= FUNDAMENTOS DE SISTEMAS EMBARCADOS =\n");
}

float menu_onoff(){
    float histerese;

    printf("Insira o valor de histerese: ");
    scanf("%f", &histerese);
    if(histerese < 0.0){
        printf("Valor inválido!\n");
        menu_onoff();
    }

    return histerese;
}

void menu_pid(){
    int escolha;

    printf("[1] Utilizar valor padrão para o PID (Kp = 5.0, Ki = 1.0, Kd = 5.0);\n");
    printf("[2] Definir valores de PID manualmente;\n");
    scanf("%d", &escolha);

    switch (escolha){
    case 1:
        Kp_ = 5.0;
        Ki_ = 1.0;
        Kd_ = 5.0;
        break;
    case 2:
        printf("Insira o valor de Kp: ");
        scanf("%f", &Kp_);
        printf("Insira o valor de Ki: ");
        scanf("%f", &Ki_);
        printf("Insira o valor de Kd: ");
        scanf("%f", &Kd_);
        break;
    default:
        printf("Opção inválida!\n");
        menu_pid();
        break;
    }
}

int menu_controle_manual(){
    int escolha;
    printf("[1] Utilizar controle ON/OFF;\n");
    printf("[2] Utilizar controle PID;\n");
    scanf("%d", &escolha);

    switch (escolha){
    case 1:
        return 1;
        break;
    case 2:
        return 2;
        break;
    default:
        printf("Opção inválida!\n");
        menu_controle_manual();
        break;
    }
}

int menu_controle(){
    int escolha;
    printf("[1] Utilizar o valor da chave para definir o controle (ON/OFF ou PID);\n");
    printf("[2] Definir controle (ON/OFF ou PID) manualmente;\n");
    printf("[3] Encerrar programa;\n");
    scanf("%d", &escolha);

    switch (escolha){
    case 1:
        return 1;
        break;
    case 2:
        return 2;
        break;
    case 3:
        desligar_ventoinha();
        desligar_resistencia();
        close(f_uart0);
        break;
    default:
        printf("Opção inválida!\n");
        menu_controle();
        break;
    }
}

void temperatura_potenciometro(){
    float histerese;
    int controle;
    int escolha = menu_controle();
    switch (escolha){
    case 1:
        histerese = menu_onoff();
        menu_pid();

        chavepotenciometro(f_uart0, histerese, Kp_, Ki_, Kd_);
        break;
    case 2:
        controle = menu_controle_manual();
        if (controle == 1){
            histerese = menu_onoff();
            onoffpotenciometro(f_uart0, histerese);
        } else {
            menu_pid();
            pidpotenciometro(f_uart0, Kp_, Ki_, Kd_);
        }
        break;
    default:
        break;
    }
}

void temperatura_manual(){
    float histerese;
    int controle;
    float TempRef = 0.0;

    printf("Insira a temperatura de referência: ");
    scanf("%f", &TempRef);
    while (TempRef < 0.0 || TempRef > 100.0){
        printf("Valor inválido! A temperatura deve estar entre 0.0 e 100.0 graus.\n");
        printf("Insira a temperatura de referência: ");
        scanf("%f", &TempRef);
    }

    int escolha = menu_controle();
    switch (escolha){
    case 1:
        histerese = menu_onoff();
        menu_pid();

        chavemanual(f_uart0, TempRef, histerese, Kp_, Ki_, Kd_);
        break;
    case 2:
        controle = menu_controle_manual();
        if (controle == 1){
            histerese = menu_onoff();
            onoffmanual(f_uart0, TempRef, histerese);
        } else {
            menu_pid();
            pidmanual(f_uart0, TempRef, Kp_, Ki_, Kd_);
        }
        break;
    default:
        break;
    }
}

void menu_referencia(){
    int escolha;
    printf("[1] Utilizar a temperatura de referência do potênciometro;\n");
    printf("[2] Definir temperatura de referência manualmente;\n");
    printf("[3] Encerrar programa;\n");
    scanf("%d", &escolha);

    switch (escolha){
    case 1:
        temperatura_potenciometro();
        break;
    case 2:
        temperatura_manual();
        break;
    case 3:
        desligar_ventoinha();
        desligar_resistencia();
        close(f_uart0);
        return;
    default:
        printf("Opção inválida!\n");
        menu_referencia();
        break;
    }
}