#include <stdlib.h>
#include <unistd.h>
#include "control_lcd_12x2.h"
#include "modbus.h"
#include "gpio.h"
#include "bme280.h"
#include "read_bme280.h"
#include "fuart.h"
#include "pid.h"
#include "csv.h"

void printar_tela(float TE, float TI, float TR, double controle);
int onoffpotenciometro(int f_uart0, float histerese);
int onoffmanual(int f_uart0, float TR, float histerese);
void pidpotenciometro(int f_uart0, double Kp_, double Ki_, double Kd_);
void pidmanual(int f_uart0, float TR, double Kp_, double Ki_, double Kd_);
int chavepotenciometro(int f_uart0, float histerese, double Kp_, double Ki_, double Kd_);
int chavemanual(int f_uart0, float TR, float histerese, double Kp_, double Ki_, double Kd_);