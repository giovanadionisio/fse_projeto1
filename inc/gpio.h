#ifndef GPIO_H_
#define GPIO_H_

void controle_resistencia_ventoinha(int sinal_controle);
void desligar_resistencia();
void desligar_ventoinha();
void ligar_ventoinha(int potencia);
void ligar_resistencia(int potencia);

#endif /* PID_H_ */

#include <wiringPi.h> 
#include <softPwm.h>

#define PIN_RESISTOR 4
#define PIN_VENTOINHA 5
#define VALOR_INICIAL 0
#define RANGE 100