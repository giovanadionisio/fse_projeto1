#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include "crc16.h"

int le_temperatura_interna(int f_uart0, float *TI);
int solicita_temperatura_interna(int f_uart0, float *TI);
int le_temperatura_potenciometro(int f_uart0, float *TR);
int solicita_temperatura_potenciometro(int f_uart0, float *TR);
int le_estado_chave(int f_uart0);
int solicita_estado_chave(int f_uart0);
int envia_sinal_controle(int f_uart0, int sinal);
