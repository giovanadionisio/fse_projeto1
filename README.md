# Fundamentos de Sistemas Embarcados
Aluna: Giovana Vitor Dionisio Santana  
Matricula: 180017659

# Instruções para compilação e execução
Dentro do repositório do projeto, em sua máquina, execute o seguinte comando no terminal:  
```make run```  
Siga as instruções do menu de acordo com o modo de execução que será utilizado.  
Para encerrar o programa, pressione  
```Ctrl + C```

# Resultado do experimento
## Controle ON/OFF
### Temperaturas
![img](https://i.ibb.co/89bS9nP/Figure-1.png)

### Sinal de Controle
![img](https://i.ibb.co/JsZhrcC/Figure-2.png)

### Legendas
![img](https://i.ibb.co/9wXr1TG/Legenda.png)

## Controle PID
### Temperaturas
![img](https://i.ibb.co/qW4rJr0/Figure-4.png)

### Sinal de Controle
![img](https://i.ibb.co/5MJhzg5/Figure-3.png)

### Legendas
![img](https://i.ibb.co/9wXr1TG/Legenda.png)
